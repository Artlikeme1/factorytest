FROM python:3.11

ENV PYTHONUNBUFFERED 1

WORKDIR /factorytest

COPY req.txt .
COPY manage.py .

RUN pip install --no-cache-dir -r req.txt

COPY . .
RUN chmod +x docker/script.sh

ENV DJANGO_ENV=production

#CMD ["gunicorn", "--bind", "0.0.0.0:8000", "factorytest.wsgi:application"]
