from __future__ import absolute_import, unicode_literals

import os
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'factorytest.settings')

app = Celery('factorytest')
app.config_from_object('django.conf:settings', namespace='CELERY')

# app.conf.enable_utc = False

app.conf.update(timezone='Europe/Paris')
app.conf.broker_connection_retry_on_startup = True

app.conf.beat_schedule = {
    'send-statistics-email-every-day': {
        'task': 'notifications.tasks.send_statistics_email',
        'schedule': crontab(minute=0, hour=12),
    },
}

app.autodiscover_tasks()
