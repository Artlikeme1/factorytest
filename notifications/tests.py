from rest_framework.test import APITestCase
from rest_framework.reverse import reverse
from rest_framework import status
from .models import Mailing, Client, Message
from django.contrib.auth.models import User


class APITestCaseBase(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='test', password='test')
        self.client1 = Client.objects.create(user=self.user,
                                             phone_number='1234567890',
                                             operator_code='123',
                                             tag='test',
                                             timezone='UTC')
        self.mailing1 = Mailing.objects.create(
            start_time='2023-07-13T15:39:11.796000Z',
            end_time='2023-07-13T15:39:11.796000Z',
            text='test text',
            filter_field='Mob. code',
            filter_value='123'
        )
        self.message1 = Message.objects.create(mailing=self.mailing1,
                                               client=self.client1,
                                               status='200'
                                               )


class ClientViewTest(APITestCaseBase):

    def test_get_all_clients(self):
        self.client.login(username='test', password='test')
        response = self.client.get(reverse('client-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_create_client(self):
        self.client.login(username='test', password='test')
        response = self.client.post(reverse('client-list'),
                                    {
                                        'phone_number': '0987654321',
                                        'operator_code': '09',
                                        'tag': 'another test',
                                        'timezone': 'EDT',
                                        'user': 1
                                    })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class MailingViewTest(APITestCaseBase):

    def test_get_all_mailings(self):
        self.client.login(username='test', password='test')
        response = self.client.get(reverse('mailing-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_get_mailing_statistics(self):
        self.client.login(username='test', password='test')
        response = self.client.get(reverse('mailing-statistics'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)


class MessageViewTest(APITestCaseBase):

    def test_get_all_messages(self):
        self.client.login(username='test', password='test')
        response = self.client.get(reverse('message-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_get_message(self):
        self.client.login(username='test', password='test')
        response = self.client.get(reverse('message-detail', kwargs={'pk': 1}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], 1)
