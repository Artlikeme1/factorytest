from django.contrib.auth.models import User
from django.db import models


class Mailing(models.Model):
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    text = models.TextField()
    filter_field = models.CharField(
        max_length=15,
        choices=(
            ("Mob. code", "Mob. code"),
            ("Tag", "Tag")
        )
    )
    filter_value = models.CharField(max_length=255)
    status = models.CharField(max_length=50, default='in_process', blank=True)
    time_interval_start_time = models.TimeField(null=True, blank=True)
    time_interval_end_time = models.TimeField(null=True, blank=True)


class Client(models.Model):
    phone_number = models.CharField(max_length=12)
    operator_code = models.CharField(max_length=12)
    tag = models.CharField(max_length=50)
    timezone = models.CharField(max_length=50)


class Message(models.Model):
    id = models.AutoField(primary_key=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=50)
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
