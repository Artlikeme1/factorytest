from django.db import transaction
from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import Mailing
from .tasks import start_mailing


@receiver(post_save, sender=Mailing)
def trigger_mailing(sender, instance, created, **kwargs):
    if created:
        transaction.on_commit(lambda: start_mailing.apply_async(args=[instance.id, ], eta=instance.start_time))
