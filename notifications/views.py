from datetime import datetime
import logging

from django.db.models import Count
from rest_framework import viewsets, mixins, status
from rest_framework.viewsets import GenericViewSet

from .models import Mailing, Client, Message
from .serializers import MailingSerializer, ClientSerializer, MessageSerializer
from .tasks import start_mailing
from rest_framework.decorators import action
from rest_framework.response import Response

logger = logging.getLogger(__name__)


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    http_method_names = ['get', 'post', 'head', 'delete']

    def create(self, request, *args, **kwargs):
        logger.info(f'Creating a new client with data: {request.data}')
        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        logger.info(f'Updating client with id: {kwargs["pk"]} with data: {request.data}')
        return super().update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        logger.info(f'Deleting client with id: {kwargs["pk"]}')
        return super().destroy(request, *args, **kwargs)


class MailingViewSet(mixins.RetrieveModelMixin,
                     mixins.UpdateModelMixin,
                     mixins.DestroyModelMixin,
                     mixins.CreateModelMixin,
                     mixins.ListModelMixin,
                     GenericViewSet):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

    def create(self, request, *args, **kwargs):
        logger.info(f'Creating a new mailing with data: {request.data}')
        return super().create(request, *args, **kwargs)
    #     serializer = self.get_serializer(data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #     self.perform_create(serializer)
    #     start_mailing.apply_async((serializer.data['id'],), eta=serializer.data['start_time'])
    #
    #     headers = self.get_success_headers(serializer.data)
    #     return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        logger.info(f'Updating mailing with id: {kwargs["pk"]} with data: {request.data}')
        return super().update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        logger.info(f'Deleting mailing with id: {kwargs["pk"]}')
        return super().destroy(request, *args, **kwargs)

    @action(detail=False)
    def completed_statistics(self, request):
        mailing = Mailing.objects.filter(status='success')
        all_stats = []
        for mail in mailing:
            messages = Message.objects.filter(mailing=mail)
            message_stats = messages.values('status').annotate(count=Count('status'))
            broadcast_stats = {
                'mailing_id': mail.id,
                'message_statistics': message_stats
            }
            all_stats.append(broadcast_stats)
        return Response(all_stats)

    @action(detail=False)
    def waiting_statistics(self, request):
        mailing = Mailing.objects.filter(status='in_process')
        all_stats = []
        for mail in mailing:
            broadcast_stats = {
                'mailing': mail.id,
                'start': str(mail.start_time.replace(tzinfo=None) - datetime.utcnow())
            }
            all_stats.append(broadcast_stats)
        return Response(all_stats)

    @action(detail=True)
    def detail_statistics(self, request, pk):
        mailing = self.get_object()
        messages = Message.objects.filter(mailing=mailing)
        message_stats = messages.values('status').annotate(count=Count('status'))
        return Response(message_stats)


class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    def create(self, request, *args, **kwargs):
        logger.info(f'Sending a new message with data: {request.data}')
        return super().create(request, *args, **kwargs)