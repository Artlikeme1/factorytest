import logging

import pytz
import requests
from django.core.mail import send_mail
from django.utils import timezone

from factorytest import settings


logger = logging.getLogger(__name__)


def send_message(client, mailing, message):
    url = f"https://probe.fbrq.cloud/v1/send/{message.id}"
    data = {
        "id": message.id,
        "phone": client.phone_number,
        "text": mailing.text
    }
    headers = {
        "accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MjA2MTM4MjAsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBzOi8vdC5tZS9hcnRsaWtlbWUifQ.36Gshfvaq9s5cYcJNPMaWdKIOT2ga1UVADUrUTcF-x0"}

    req = requests.post(url, json=data, headers=headers)
    logger.info(f'Response code: {req.status_code} Mailing: {mailing.id}, message: {message.id}')
    return req


def send_daily_mail(data):
    send_mail(
        subject='Test',
        message=data,
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=['test1@test.test'],
        fail_silently=False,
    )
    logger.info(f'Success send daily report!')


def check_time_interval(client, mailing):
    now_utc = timezone.now()
    try:
        client_tz = pytz.timezone(client.timezone)
    except Exception:
        return False
    local_time = now_utc.astimezone(client_tz)

    return mailing.time_interval_start_time <= local_time.time() <= mailing.time_interval_end_time
