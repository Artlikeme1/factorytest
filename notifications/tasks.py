import logging
import time
from datetime import timedelta

from django.db.models import Count
from factorytest.celery import app
from django.utils import timezone

from notifications.models import Mailing, Client, Message
from notifications.services import send_message, send_daily_mail, check_time_interval

logger = logging.getLogger(__name__)


@app.task()
def start_mailing(mailing_id):
    mailing = Mailing.objects.get(id=mailing_id)
    if mailing.filter_field == 'Tag':
        clients = Client.objects.filter(tag=mailing.filter_value)
    else:
        clients = Client.objects.filter(operator_code=mailing.filter_value)

    for client in clients:
        print(check_time_interval(client, mailing))
        if mailing.start_time <= timezone.now() + timedelta(seconds=2) <= mailing.end_time \
                and check_time_interval(client, mailing):
            try:
                message = Message.objects.create(
                    mailing=mailing,
                    client=client,
                    status="None",
                )
                response = send_message(client, mailing, message)
                message.status = response.status_code
                message.save()

            except Exception as e:
                logger.info(f"Error message to client {client.id}: {e}")

    logger.info(f'Success end of mailing with id: {mailing_id}')
    mailing.status = 'success'
    mailing.save()


@app.task()
def send_statistics_email():
    mailings = Mailing.objects.all()
    data = []
    for mailing in mailings:
        message_stats = Message.objects.filter(mailing=mailing) \
            .values('status') \
            .annotate(count=Count('status'))
        data.append({
            'mailing_id': mailing.id,
            'message_statistics': list(message_stats)
        })
    send_daily_mail(data)
