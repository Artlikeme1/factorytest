
# Test Task for FactorySolutions
Cервис управления рассылками API администрирования и получения статистики.


## Run Locally

Clone the project

```bash
  git clone https://link-to-project
```

Go to the project directory

```bash
  cd my-project
```

Install dependencies

```bash
  python -m venv venv
  source venv/bin/activate
  pip install -r req.txt
```

Start the server

```bash
  python manage.py migate
  python manage.py runserver
```

Запустить redis

```bash
  redis-server
```

Запустить Celery, Beat, flower в разных терминалах

```bash
  celery -A factorytest worker -l info
```

```bash
  celery -A factorytest beat -l info
```

```bash
  celery -A factorytest flower
```

## Test_models for swagger page
Client(Url(POST): /clients/): 

```json
  {
  "phone_number": "89111919191",
  "operator_code": "7",
  "tag": "111",
  "timezone": "Europe/Paris"
  }
```

Mailing (Url(POST): /mailing/):
```json
  {
  "start_time": "2023-07-17T11:57:39.675Z",
  "end_time": "2024-07-17T11:57:39.675Z",
  "text": "string",
  "filter_field": "Mob. code",
  "filter_value": "7",
  "status": "None",
  "time_interval_start_time": "00:00:00",
  "time_interval_end_time": "19:00:00"
}
```

Статистика на адресах:
Выполненные рассылки: /mailing/completed_statistics/
Ожидающие выполнения рассылки: /mailing/waiting_statistics/
Детальная статистика конкретной рассылки: /mailing/detail_statistics/

Дополнительные выполненные пункты: 1, 5, 8, 11, 12

По всем вопросам в реализации и т.д. рад ответить в Telegram!